/**
  * Created by macblady on 27.03.2017.
  */

def doChore(chore: String): String = chore match {
  case "clean dishes" => "scrub, dry"
  case "cook dinner" => "chop, sizzle"
  case _ => "whine, complain"
}
println(doChore("clean dishes"))
println(doChore("mow lawn"))

def factorial(n: Int): Int = n match {
  case 0 => 1
  case x if x > 0 => factorial(n - 1) * n
}

println(factorial(3))
println(factorial(0))


val reg = """^(F|f)\w*""".r
println(reg.findFirstIn("Fantastic"))
println(reg.findFirstIn("not Fantastic"))

val reg2 = """the""".r
println(reg2.findAllIn("the way the scissors trim the hair and the shrubs").toList)

val movies = <movies>
  <movie>Inglorouis Basterds</movie>
  <movie>The Wolf of Wall Street</movie>
  <trilogy>Indiana Jones</trilogy>
  <trilogy>Godfather</trilogy>
</movies>

(movies \ "_").foreach { movie =>
  movie match {
    case <movie>{movieName}</movie> => println(movieName)
    case <trilogy>{trilogyName}</trilogy> => println(trilogyName + " (trilogy)")
  }
}
