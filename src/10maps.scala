/**
  * Created by macblady on 27.03.2017.
  */

import scala.collection.mutable

val ordinals = Map(0 -> "zero", 1 -> "one", 2 -> "two")
println(ordinals(2))


val map = new mutable.HashMap[Int, String]

map += 4 -> "four"
map += 8 -> "eight"

println(map)

val hobbits = Map("frodo"->"hobbit", "samwise"->"hobbit", "pippin"->"hobbit")
hobbits.foreach(hobbit => println(hobbit))
hobbits.foreach(hobbit => println(hobbit._1))
