/**
  * Created by macblady on 20.03.2017.
  */


object Game {
  val players = ("O", "X")
  val emptyCell = "-"

  def printArr(board: Array[Array[String]]): Unit = {
    for (row <- board) {
      var line = ""
      for (item <- row) {
        line = line + item
      }
      println(line)
    }
  }

  def checkCombination(combination: Array[String]): Boolean = {
    val first = combination(0)
    if (first.eq(emptyCell)) {
      return false
    }
    else if (combination(1).eq(first) && combination(2).eq(first)) {
      return true
    } else {
      return false
    }
  }

  def isFinished(board: Array[Array[String]]): Boolean = {
    val combinations = Array(
      Array(board(0)(0), board(0)(1), board(0)(2)),
      Array(board(1)(0), board(1)(1), board(1)(2)),
      Array(board(2)(0), board(2)(1), board(2)(2)),
      Array(board(0)(0), board(1)(0), board(2)(0)),
      Array(board(0)(1), board(1)(1), board(2)(1)),
      Array(board(0)(2), board(1)(2), board(2)(2)),
      Array(board(0)(0), board(1)(1), board(2)(2)),
      Array(board(2)(0), board(1)(1), board(0)(2))
    )

    var isFinished = false
    for (combination <- combinations) {
      isFinished = checkCombination(combination)
      if (isFinished) {
        return true
      }
    }
    return false
  }

  def isMovePossible(move: Array[Int], board: Array[Array[String]]): Boolean = {
    if (board(move(0))(move(1)).eq(emptyCell)) {
      return true
    }
    return false

  }

  def isInRange(x: Int): Boolean = {
    if (x >= 0 && x <= 2) {
      return true
    }
    return false
  }

  def run(): Unit = {
    val board = Array(
      Array(emptyCell, emptyCell, emptyCell),
      Array(emptyCell, emptyCell, emptyCell),
      Array(emptyCell, emptyCell, emptyCell)
    )

    var isOver = false

    var currentPlayer = players._1

    while(!isOver) {
      var accepted = false
      var row = -1
      var col = -1
      while (!accepted) {
        println("Player " +  currentPlayer + " row: ")
        row = Console.readInt()
        println("Player " + currentPlayer + " column: ")
        col = Console.readInt()
        if (!isInRange(row) || !isInRange(col)) {
          println("Move outside the board is not possible!")

        } else {
          accepted = isMovePossible(Array(row, col), board)
          if (!accepted) {
            println("Position already used! Please select once again!")
          }
        }
      }

      board(row)(col) = currentPlayer
      this.printArr(board)

      if (isFinished(board)) {
        println(s"Congratulations for: $currentPlayer. You won!")
        isOver = true
      }

      if (currentPlayer == players._1) {
        currentPlayer = players._2
      }
      else {
        currentPlayer = players._1
      }
    }
  }
}

Game.run()
