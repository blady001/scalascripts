/**
  * Created by macblady on 27.03.2017.
  */

var lista = List("one", "two", "three")

println(lista(2))

println(Nil)

var a = List(1,2,3)
var b = List(3,2,1)

if (a == b) {
  println("True")
}
else {
  println("False")
}

var list = List("frodo", "samwise", "pippin")
list.foreach(hobbit => println(hobbit))

println(list.isEmpty)
println(Nil.isEmpty)
println(list.size)

println(list.head)
println(list.tail)
println(list.last)
println(list.init)

println(list.reverse)
println(list.drop(1))

val words = List("peg", "al", "bud", "kelly")

println(words.count(word => word.size > 2))
println(words.filter(word => word.size > 2))
println(words.map(word => word.size))

println(words.forall(word => word.size > 1))
println(words.forall(word => word.size > 4))
println(words.forall(word => word.size > 5))

println(words.sortWith(_ < _))

// Sumowanie na 2 sposoby
val lista2 = List(1, 2, 3)
val sum = (0 /: lista2) {(sum, i) => sum + i} // mapowanie do sum, i
println(sum)
println(lista2.foldLeft(1)((sum, value) => sum + value))
