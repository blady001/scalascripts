/**
  * Created by macblady on 27.03.2017.
  */

class Person(val name: String)

trait Nice {
  def greet() = println("Howdily doodily.")
}

class Character(override val name: String) extends Person(name) with Nice

val flanders = new Character("Ned")
flanders.greet
