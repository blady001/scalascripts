/**
  * Created by macblady on 20.03.2017.
  */

var sample: Int = _

val text = "abcd"

println(text)

var number: Int = 4.*(2)

println(number)

sample = 10
println(sample)

val (var1: Int, var2: String) = (2, "abcd")

println(var1)

println("size: " + text.size)

println("Nil: " + Nil)
